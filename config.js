// noinspection JSUnresolvedVariable
module.exports = {
    endpoint: process.env.CI_API_V4_URL,
    hostRules: [
        {
            baseUrl: 'https://gitlab.com/api/v4/',
            token: process.env.CI_JOB_TOKEN,
        },
    ],
    platform: 'gitlab',
    trustLevel: 'high',
    onboarding: true,
    repositories: [
        'amuttsch_cc/renovate-yarn-husky',
    ],
};

